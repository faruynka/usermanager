# Description:

UserManager is a microservice which works with users personal data. Together with Messanger microservice it provides "forum" functionality. 
There are two types of users: user and administrator. Administrators have rigth to work with all users data, and users can edit only
their own parameters. There are also some differences in how the user and the administrator can interact with messages, you can read more about it in
Messanger [README](https://bitbucket.org/faruynka/messenger/src/master/README.md).

# Technologies used:

* spring boot
* migrations-flyway
* microservices-feign client
* test-junit
* mockito
* db-postgress
* spring data jpa.
