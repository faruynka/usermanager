create sequence user_generator start 1 increment 1;

create table users (
	id int8 not null,
	username varchar(255),
	real_username varchar(255),
	password varchar(255),
	email varchar(255),
  is_deleted boolean,
  role varchar(255),
  primary key (id)
);

insert into users (id, username, real_username, password, email, is_deleted, role)
  values (1, 'admin', 'admin', '123456', 'admin@gmail.com', false, 'ADMIN');