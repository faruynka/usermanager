package com.user_manager.dto;

import com.user_manager.domain.Role;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class UserDto {
    private Long id;

    @NotBlank(message = "user name is required")
    @Size(min = 3, message = "name size should be 3 or more symbols")
    private String username;

    @NotBlank(message = "password is required")
    @Size(min = 6, max = 30, message = "password should be at least more than 5 symbols and less than 31 symbols")
    private String password;

    @Email(message = "email is incorrect")
    private String email;

    @Enumerated(EnumType.STRING)
    private Role role;

    public UserDto() {
    }

    public void setRole(Role role) {
        if (role == null) {
            role = Role.USER;
        }
        this.role = role;
    }
}
