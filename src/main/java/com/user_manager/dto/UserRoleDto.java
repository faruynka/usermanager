package com.user_manager.dto;

import com.user_manager.domain.Role;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Data
public class UserRoleDto {
    @NotNull
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role;
}
