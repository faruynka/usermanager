package com.user_manager.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class AuthenticateUserRequestDto {
    @NotBlank(message = "user name is required")
    @Size(min = 3, message = "name size should be 3 or more symbols")
    private String username;

    @NotBlank(message = "password is required")
    @Size(min = 6, max = 30, message = "password should be at least more than 5 symbols and less than 31 symbols")
    private String password;

    public AuthenticateUserRequestDto() {
    }
}

