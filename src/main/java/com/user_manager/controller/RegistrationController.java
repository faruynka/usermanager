package com.user_manager.controller;

import com.user_manager.domain.User;
import com.user_manager.dto.RequestUserDto;
import com.user_manager.dto.UserDto;
import com.user_manager.service.UserService;
import com.user_manager.transformer.Transformer;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/registration")
public class RegistrationController {
    private final UserService userService;
    private final Transformer<User, RequestUserDto> requestUserTransformer;
    private final Transformer<User, UserDto> userTransformer;

    public RegistrationController(UserService userService, Transformer<User, RequestUserDto> requestUserTransformer,
                                  Transformer<User, UserDto> userTransformer) {
        this.userService = userService;
        this.requestUserTransformer = requestUserTransformer;
        this.userTransformer = userTransformer;
    }

    @GetMapping
    public String register() {
        return "Hello user. Register. please";
    }

    @PostMapping
    public UserDto add(@Valid @RequestBody RequestUserDto requestUserDto) {
        User user = userService.add(requestUserTransformer.parseToEntity(requestUserDto),
                requestUserDto.getRepeatedPassword());
        return userTransformer.parseToDto(user);
    }

}
