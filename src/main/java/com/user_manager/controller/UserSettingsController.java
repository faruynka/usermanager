package com.user_manager.controller;

import com.user_manager.domain.User;
import com.user_manager.dto.UserRoleDto;
import com.user_manager.dto.PasswordUserValidationDto;
import com.user_manager.dto.UserDto;
import com.user_manager.service.UserService;
import com.user_manager.transformer.Transformer;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.nio.file.AccessDeniedException;

@RestController
@RequestMapping("/settings/user")
public class UserSettingsController {
    private final UserService userService;
    private final Transformer<User, UserDto> userTransformer;
    private final Transformer<User, PasswordUserValidationDto> passwordValidateTransformer;


    public UserSettingsController(UserService userService, Transformer<User, UserDto> userTransformer,
                                  Transformer<User, PasswordUserValidationDto> passwordValidateTransformer) {
        this.userService = userService;
        this.userTransformer = userTransformer;
        this.passwordValidateTransformer = passwordValidateTransformer;
    }

    @GetMapping
    public UserDto get(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId, @Valid @RequestBody UserDto userDto)
            throws AccessDeniedException {
        User user = userService.find(userId, userDto.getId());
        return userTransformer.parseToDto(user);
    }

    @GetMapping("/get")
    public UserDto get(@RequestParam Long userId) {
        return userTransformer.parseToDto(userService.find(userId));
    }

    @PostMapping("/edit")
    public UserDto editUser(@RequestHeader(HttpHeaders.AUTHORIZATION) Long currentUserId,
                            @Valid @RequestBody UserDto userDto) throws AccessDeniedException {
        User changedUser = userService.edit(currentUserId, userTransformer.parseToEntity(userDto));
        return userTransformer.parseToDto(changedUser);
    }

    @DeleteMapping("/delete")
    public UserDto deleteUser(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId,
                              @Valid @RequestBody UserDto userDto)
            throws AccessDeniedException {
        User user = userService.delete(userId, userDto.getId(), userDto.getPassword());
        return userTransformer.parseToDto(user);
    }

    @PutMapping("/recover")
    public UserDto recover(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId,
                           @Valid @RequestBody PasswordUserValidationDto userDto) throws AccessDeniedException {
        User user = userService.recover(userId, passwordValidateTransformer.parseToEntity(userDto),
                userDto.getRepeatedPassword());
        return userTransformer.parseToDto(user);
    }

    @PutMapping("/edit/role")
    public UserDto recover(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId,
                           @Valid @RequestBody UserRoleDto userRoleDto) throws AccessDeniedException {
        return userTransformer.parseToDto(userService.changeRole(userId, userRoleDto.getId(), userRoleDto.getRole()));
    }
}
