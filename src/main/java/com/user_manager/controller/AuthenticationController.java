package com.user_manager.controller;

import com.user_manager.domain.User;
import com.user_manager.dto.AuthenticateUserRequestDto;
import com.user_manager.dto.UserDto;
import com.user_manager.service.AuthenticationService;
import com.user_manager.transformer.Transformer;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/authentication")
public class AuthenticationController {
    private final AuthenticationService authenticationService;
    private final Transformer<User, AuthenticateUserRequestDto> authenticateUserTransformer;
    private final Transformer<User, UserDto> userTransformer;

    public AuthenticationController(AuthenticationService authenticationService,
                                    Transformer<User, AuthenticateUserRequestDto> authenticateUserTransformer,
                                    Transformer<User, UserDto> userTransformer) {
        this.authenticationService = authenticationService;
        this.authenticateUserTransformer = authenticateUserTransformer;
        this.userTransformer = userTransformer;
    }

    @GetMapping
    public String login() {
        return "Hello user. Log in, please.";
    }

    @PostMapping("/login")
    public UserDto login(@Valid @RequestBody AuthenticateUserRequestDto requestUserDto) {
        User user = authenticationService.login(authenticateUserTransformer.parseToEntity(requestUserDto));
        return userTransformer.parseToDto(user);
    }
}
