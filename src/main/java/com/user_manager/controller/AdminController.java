package com.user_manager.controller;

import com.user_manager.domain.User;
import com.user_manager.dto.RequestUserDto;
import com.user_manager.service.UserService;
import com.user_manager.transformer.Transformer;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.nio.file.AccessDeniedException;
import java.util.List;

@RestController
@RequestMapping("/admin")
public class AdminController {
    private final UserService userService;
    private final Transformer<User, RequestUserDto> userTransformer;

    public AdminController(UserService userService, Transformer<User, RequestUserDto> userTransformer) {
        this.userService = userService;
        this.userTransformer = userTransformer;
    }

    @GetMapping
    public List<RequestUserDto> getUsers(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId)
            throws AccessDeniedException {
        return userTransformer.parseToDto(userService.findActive(userId));
    }
}
