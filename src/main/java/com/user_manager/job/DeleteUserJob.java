package com.user_manager.job;

import com.user_manager.service.UserService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class DeleteUserJob {
    private final UserService userService;

    public DeleteUserJob(UserService userService) {
        this.userService = userService;
    }

    @Scheduled(cron = "0 0 12 */7 * ?")
    public void deleteUsers() {
        userService.deleteAllByStatus();
    }
}

