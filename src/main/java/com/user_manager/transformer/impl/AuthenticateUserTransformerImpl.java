package com.user_manager.transformer.impl;

import com.user_manager.domain.User;
import com.user_manager.dto.AuthenticateUserRequestDto;
import com.user_manager.transformer.Transformer;
import org.springframework.stereotype.Component;

@Component
public class AuthenticateUserTransformerImpl implements Transformer<User, AuthenticateUserRequestDto> {
    @Override
    public User parseToEntity(User user, AuthenticateUserRequestDto requestUserDTO) {
        if (requestUserDTO == null) {
            return null;
        }
        if (user == null) {
            user = new User();
        }
        if (requestUserDTO.getUsername() != null) {
            user.setUsername(requestUserDTO.getUsername());
        }
        if (requestUserDTO.getPassword() != null) {
            user.setPassword(requestUserDTO.getPassword());
        }
        return user;
    }

    @Override
    public AuthenticateUserRequestDto parseToDto(User user) {
        if (user == null) {
            return null;
        }
        AuthenticateUserRequestDto requestUserDTO = new AuthenticateUserRequestDto();
        requestUserDTO.setUsername(user.getUsername());
        requestUserDTO.setPassword(user.getPassword());
        return requestUserDTO;
    }
}
