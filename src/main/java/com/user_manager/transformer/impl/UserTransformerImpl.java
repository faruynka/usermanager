package com.user_manager.transformer.impl;

import com.user_manager.domain.User;
import com.user_manager.dto.UserDto;
import com.user_manager.transformer.Transformer;
import org.springframework.stereotype.Component;

@Component
public class UserTransformerImpl implements Transformer<User, UserDto> {
    @Override
    public User parseToEntity(User user, UserDto userDTO) {
        if (userDTO == null) {
            return null;
        }
        if (user == null) {
            user = new User();
        }
        if (userDTO.getId() != null) {
            user.setId(userDTO.getId());
        }
        if (userDTO.getUsername() != null) {
            user.setUsername(userDTO.getUsername());
        }
        if (userDTO.getPassword() != null) {
            user.setPassword(userDTO.getPassword());
        }
        if (userDTO.getEmail() != null) {
            user.setEmail(userDTO.getEmail());
        }
        if (userDTO.getRole() != null) {
            user.setRole(userDTO.getRole());
        }
        user.setRole(userDTO.getRole());
        return user;
    }

    @Override
    public UserDto parseToDto(User user) {
        if (user == null) {
            return null;
        }
        UserDto requestUserDto = new UserDto();
        requestUserDto.setId(user.getId());
        requestUserDto.setUsername(user.getUsername());
        requestUserDto.setPassword(user.getPassword());
        requestUserDto.setEmail(user.getEmail());
        requestUserDto.setRole(user.getRole());
        return requestUserDto;
    }
}
