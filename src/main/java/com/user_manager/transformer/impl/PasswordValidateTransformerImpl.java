package com.user_manager.transformer.impl;

import com.user_manager.domain.User;
import com.user_manager.dto.PasswordUserValidationDto;
import com.user_manager.transformer.Transformer;
import org.springframework.stereotype.Component;

@Component
public class PasswordValidateTransformerImpl implements Transformer<User, PasswordUserValidationDto> {
    @Override
    public User parseToEntity(User user, PasswordUserValidationDto userDTO) {
        if (userDTO == null) {
            return null;
        }
        if (user == null) {
            user = new User();
        }
        if (userDTO.getUsername() != null) {
            user.setUsername(userDTO.getUsername());
        }
        if (userDTO.getPassword() != null) {
            user.setPassword(userDTO.getPassword());
        }
        return user;
    }

    @Override
    public PasswordUserValidationDto parseToDto(User user) {
        if (user == null) {
            return null;
        }
        PasswordUserValidationDto requestUserDTO = new PasswordUserValidationDto();
        requestUserDTO.setUsername(user.getUsername());
        requestUserDTO.setPassword(user.getPassword());
        return requestUserDTO;
    }
}
