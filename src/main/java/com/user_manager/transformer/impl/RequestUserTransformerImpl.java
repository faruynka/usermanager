package com.user_manager.transformer.impl;

import com.user_manager.domain.User;
import com.user_manager.dto.RequestUserDto;
import com.user_manager.transformer.Transformer;
import org.springframework.stereotype.Component;

@Component
public class RequestUserTransformerImpl implements Transformer<User, RequestUserDto> {
    @Override
    public User parseToEntity(User user, RequestUserDto requestUserDTO) {
        if (requestUserDTO == null) {
            return null;
        }
        if (user == null) {
            user = new User();
        }
        if (requestUserDTO.getId() != null) {
            user.setId(requestUserDTO.getId());
        }
        if (requestUserDTO.getUsername() != null) {
            user.setUsername(requestUserDTO.getUsername());
        }
        if (requestUserDTO.getPassword() != null) {
            user.setPassword(requestUserDTO.getPassword());
        }
        if (requestUserDTO.getEmail() != null) {
            user.setEmail(requestUserDTO.getEmail());
        }
        user.setRole(requestUserDTO.getRole());
        return user;
    }

    @Override
    public RequestUserDto parseToDto(User user) {
        if (user == null) {
            return null;
        }
        RequestUserDto requestUserDTO = new RequestUserDto();
        requestUserDTO.setId(user.getId());
        requestUserDTO.setUsername(user.getUsername());
        requestUserDTO.setPassword(user.getPassword());
        requestUserDTO.setEmail(user.getEmail());
        requestUserDTO.setRole(user.getRole());
        return requestUserDTO;
    }
}
