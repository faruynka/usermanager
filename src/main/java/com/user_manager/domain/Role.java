package com.user_manager.domain;

public enum Role {
    USER,
    ADMIN
}
