package com.user_manager.repo;


import com.user_manager.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Long> {
    Optional<User> findByIdAndIsDeletedFalse(Long id);

    Optional<User> findByUsernameAndIsDeletedFalse(String username);

    Optional<User> findByRealUsername(String username);

    @Modifying
    @Query("update User u set u.username = 'DELETED',u.isDeleted = true where u.id = :id")
    User setDeletedById(@Param("id") Long id);

    List<User> findAllByIsDeletedIsFalse();

    void deleteByIsDeletedTrue();
}
