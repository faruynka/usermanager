package com.user_manager.service;

import com.user_manager.domain.Role;
import com.user_manager.domain.User;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.AccessDeniedException;
import java.util.List;

public interface UserService {
    User add(User user, String repeatedPassword);

    User edit(Long currentUserId, User user) throws AccessDeniedException;

    User delete(Long currentUserId, Long userToDeleteId, String password) throws AccessDeniedException;

    void deleteAllByStatus();

    @Transactional
    User find(Long id);

    User find(Long currentUserId, Long id) throws AccessDeniedException;

    List<User> findActive(Long currentUserId) throws AccessDeniedException;

    User recover(Long currentUserId, User user, String repeatedPassword) throws AccessDeniedException;

    User changeRole(Long currentUser, Long userId, Role role) throws AccessDeniedException;
}
