package com.user_manager.service.impl;

import com.user_manager.domain.User;
import com.user_manager.exception.PasswordException;
import com.user_manager.repo.UserRepo;
import com.user_manager.service.AuthenticationService;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    private final UserRepo userRepo;

    public AuthenticationServiceImpl(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public User login(User user) {
        User userFromDB = userRepo.findByUsernameAndIsDeletedFalse(user.getUsername()).orElseThrow(()
                -> new EntityNotFoundException("there are no user with such login = " + user.getUsername()));
        if (!user.getPassword().equals(userFromDB.getPassword())) {
            throw new PasswordException();
        }
        return userFromDB;
    }
}