package com.user_manager.service.impl;

import com.user_manager.domain.Role;
import com.user_manager.domain.User;
import com.user_manager.exception.BadRequestException;
import com.user_manager.exception.PasswordException;
import com.user_manager.repo.UserRepo;
import com.user_manager.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.nio.file.AccessDeniedException;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepo userRepo;

    public UserServiceImpl(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    @Transactional
    public User find(Long id) {
        return findImpl(id);
    }

    @Override
    @Transactional
    public User find(Long currentUserId, Long id) throws AccessDeniedException {
        hasAccessRights(currentUserId, id);
        return userRepo.findByIdAndIsDeletedFalse(id).orElseThrow(()
                -> new EntityNotFoundException("there are no user with such id = " + id));
    }

    @Override
    @Transactional
    public List<User> findActive(Long currentUserId) throws AccessDeniedException {
        User user = findImpl(currentUserId);
        if (!user.getRole().equals(Role.ADMIN)) {
            throw new AccessDeniedException("User role has to be ADMIN.");
        }
        return userRepo.findAllByIsDeletedIsFalse();
    }

    @Override
    @Transactional
    public User add(User user, String repeatedPassword) {
        checkPasswordsEquality(user.getPassword(), repeatedPassword);
        if (userRepo.findByUsernameAndIsDeletedFalse(user.getUsername()).isPresent()) {
            throw new EntityNotFoundException("user already exists with such login =" + user.getUsername());
        }
        return userRepo.save(user);
    }

    @Override
    public User edit(Long currentUserId, User user) throws AccessDeniedException {
        hasAccessRights(currentUserId, user.getId());
        User userFromDB = findImpl(user.getId());
        if (user.getUsername().equals(userFromDB.getUsername()) && user.getPassword().equals(userFromDB.getPassword())
                && user.getEmail().equals(userFromDB.getEmail())) {
            throw new BadRequestException("nothing to change");
        }
        userFromDB.setUsername(user.getUsername());
        userFromDB.setPassword(user.getPassword());
        userFromDB.setEmail(user.getEmail());
        return userRepo.save(userFromDB);
    }

    @Override
    @Transactional
    public User delete(Long currentUserId, Long userToDeleteId, String password) throws AccessDeniedException {
        hasAccessRights(currentUserId, userToDeleteId);
        User userToDelete = findImpl(userToDeleteId);
        checkPasswordsEquality(password, userToDelete.getPassword());
        userToDelete.setUsername("DELETED");
        userToDelete.setDeleted(true);
        return userRepo.save(userToDelete);
    }

    @Override
    @Transactional
    public void deleteAllByStatus() {
        userRepo.deleteByIsDeletedTrue();
    }

    @Override
    @Transactional
    public User recover(Long currentUserId, User user, String repeatedPassword) throws AccessDeniedException {
        hasAccessRights(currentUserId, user.getId());
        checkPasswordsEquality(user.getPassword(), repeatedPassword);
        User userToRecover = find(user.getUsername());
        if (!userToRecover.getDeleted()) {
            throw new BadRequestException("Can't recover. User is active");
        }
        userToRecover.setUsername(user.getRealUsername());
        userToRecover.setDeleted(false);
        return userRepo.save(userToRecover);
    }

    @Override
    public User changeRole(Long currentUserId, Long userId, Role role) throws AccessDeniedException {
        User currentUser = findImpl(currentUserId);
        if (!currentUser.getRole().equals(Role.ADMIN)) {
            throw new AccessDeniedException("permission is denied");
        }
        User user = findImpl(userId);
        user.setRole(role);
        return userRepo.save(user);
    }

    private void hasAccessRights(Long currentUserId, Long userToChangeId) throws AccessDeniedException {
        if (!findImpl(currentUserId).getRole().equals(Role.ADMIN) && !currentUserId.equals(userToChangeId)) {
            throw new AccessDeniedException("permission is denied");
        }
    }

    private void checkPasswordsEquality(String password, String repeatedPassword) {
        if (!password.equals(repeatedPassword)) {
            throw new PasswordException();
        }
    }

    private User find(String userName) {
        return userRepo.findByRealUsername(userName).orElseThrow(()
                -> new EntityNotFoundException("there are no user with such login = " + userName));
    }

    private User findImpl(Long id) {
        return userRepo.findByIdAndIsDeletedFalse(id).orElseThrow(()
                -> new EntityNotFoundException("there are no user with such id = " + id));
    }
}
