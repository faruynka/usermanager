package com.user_manager.service;

import com.user_manager.domain.User;

public interface AuthenticationService {
    User login(User user);
}
