package com.user_manager.utils;

import com.user_manager.domain.Role;
import com.user_manager.domain.User;
import com.user_manager.dto.AuthenticateUserRequestDto;
import com.user_manager.dto.PasswordUserValidationDto;
import com.user_manager.dto.RequestUserDto;
import com.user_manager.dto.UserDto;

public class TestUtils {
    public static final AuthenticateUserRequestDto AUTHENTICATE_MYUSER0_REQUEST_DTO;
    public static final User MYUSER0;
    public static final RequestUserDto REQUEST_MYUSER0_DTO;
    public static final UserDto MYUSER0_DTO;
    public static final PasswordUserValidationDto PASSWORD_VALIDATION_MYUSER0_DTO;

    public static final AuthenticateUserRequestDto AUTHENTICATE_MYUSER1_REQUEST_DTO;
    public static final User MYUSER1;
    public static final RequestUserDto REQUEST_MYUSER1_DTO;
    public static final UserDto MYUSER1_DTO;
    public static final PasswordUserValidationDto PASSWORD_VALIDATION_MYUSER1_DTO;

    public static final AuthenticateUserRequestDto AUTHENTICATE_MYUSER2_REQUEST_DTO;
    public static final User MYUSER2;
    public static final RequestUserDto REQUEST_MYUSER2_DTO;
    public static final UserDto MYUSER2_DTO;
    public static final PasswordUserValidationDto PASSWORD_VALIDATION_MYUSER2_DTO;

    static {
        AUTHENTICATE_MYUSER0_REQUEST_DTO = new AuthenticateUserRequestDto("myname0", "1234560");
        MYUSER0 = new User(1L, AUTHENTICATE_MYUSER0_REQUEST_DTO.getUsername(),
                AUTHENTICATE_MYUSER0_REQUEST_DTO.getUsername(), AUTHENTICATE_MYUSER0_REQUEST_DTO.getPassword(),
                AUTHENTICATE_MYUSER0_REQUEST_DTO.getUsername() + "@gmail.com", false, Role.USER);
        REQUEST_MYUSER0_DTO = new RequestUserDto(MYUSER0.getId(), MYUSER0.getUsername(),
                MYUSER0.getPassword(), MYUSER0.getPassword(),
                MYUSER0.getEmail(), MYUSER0.getRole());
        MYUSER0_DTO = new UserDto(MYUSER0.getId(), MYUSER0.getUsername(), MYUSER0.getPassword(), MYUSER0.getEmail(),
                MYUSER0.getRole());
        PASSWORD_VALIDATION_MYUSER0_DTO = new PasswordUserValidationDto(MYUSER0.getUsername(), MYUSER0.getPassword(),
                MYUSER0.getPassword());

        AUTHENTICATE_MYUSER1_REQUEST_DTO = new AuthenticateUserRequestDto("myname1", "1234561");
        MYUSER1 = new User(2L, AUTHENTICATE_MYUSER1_REQUEST_DTO.getUsername(),
                AUTHENTICATE_MYUSER1_REQUEST_DTO.getUsername(), AUTHENTICATE_MYUSER1_REQUEST_DTO.getPassword(),
                AUTHENTICATE_MYUSER1_REQUEST_DTO.getUsername() + "@gmail.com", false, Role.USER);
        REQUEST_MYUSER1_DTO = new RequestUserDto(MYUSER1.getId(), MYUSER1.getUsername(),
                MYUSER1.getPassword(), MYUSER1.getPassword(),
                MYUSER1.getEmail(), MYUSER1.getRole());
        MYUSER1_DTO = new UserDto(MYUSER1.getId(), MYUSER1.getUsername(), MYUSER1.getPassword(), MYUSER1.getEmail(),
                MYUSER1.getRole());
        PASSWORD_VALIDATION_MYUSER1_DTO = new PasswordUserValidationDto(MYUSER1.getUsername(), MYUSER1.getPassword(),
                MYUSER1.getPassword());

        AUTHENTICATE_MYUSER2_REQUEST_DTO = new AuthenticateUserRequestDto("myname2", "1234562");
        MYUSER2 = new User(3L, AUTHENTICATE_MYUSER2_REQUEST_DTO.getUsername(),
                AUTHENTICATE_MYUSER2_REQUEST_DTO.getUsername(), AUTHENTICATE_MYUSER2_REQUEST_DTO.getPassword(),
                AUTHENTICATE_MYUSER2_REQUEST_DTO.getUsername() + "@gmail.com", false, Role.ADMIN);
        REQUEST_MYUSER2_DTO = new RequestUserDto(MYUSER2.getId(), MYUSER2.getUsername(),
                MYUSER2.getPassword(), MYUSER2.getPassword(),
                MYUSER2.getEmail(), MYUSER2.getRole());
        MYUSER2_DTO = new UserDto(MYUSER2.getId(), MYUSER2.getUsername(), MYUSER2.getPassword(), MYUSER2.getEmail(),
                MYUSER2.getRole());
        PASSWORD_VALIDATION_MYUSER2_DTO = new PasswordUserValidationDto(MYUSER2.getUsername(), MYUSER2.getPassword(),
                MYUSER2.getPassword());
    }
}
