package com.user_manager.controller;

import com.user_manager.domain.User;
import com.user_manager.dto.RequestUserDto;
import com.user_manager.dto.UserDto;
import com.user_manager.service.UserService;
import com.user_manager.transformer.Transformer;
import com.user_manager.utils.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@WebMvcTest(RegistrationController.class)
class RegistrationControllerTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private RegistrationController controller;

    @MockBean
    private UserService userService;
    @MockBean
    private Transformer<User, RequestUserDto> requestUserTransformer;
    @MockBean
    private Transformer<User, UserDto> userTransformer;

    @Test
    void registerTest() throws Exception {
        mvc.perform(get("/registration"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello user. Register. please")));
    }

    @Test
    void addTest() throws Exception {
        String userRequest = "{\"id\": \"" + TestUtils.REQUEST_MYUSER0_DTO.getId() + "\"," +
                "\"username\": \"" + TestUtils.REQUEST_MYUSER0_DTO.getUsername() + "\"," +
                " \"password\" : \"" + TestUtils.REQUEST_MYUSER0_DTO.getPassword() + "\"," +
                " \"repeatedPassword\" : \"" + TestUtils.REQUEST_MYUSER0_DTO.getRepeatedPassword() + "\"," +
                " \"email\" : \"" + TestUtils.REQUEST_MYUSER0_DTO.getEmail() + "\"," +
                " \"role\" : \"" + TestUtils.REQUEST_MYUSER0_DTO.getRole() + "\"}";

        given(requestUserTransformer.parseToEntity(TestUtils.REQUEST_MYUSER0_DTO))
                .willReturn(TestUtils.MYUSER0);
        given(userService.add(TestUtils.MYUSER0, TestUtils.REQUEST_MYUSER0_DTO.getRepeatedPassword()))
                .willReturn(TestUtils.MYUSER0);
        given(userTransformer.parseToDto(TestUtils.MYUSER0)).willReturn(TestUtils.MYUSER0_DTO);

        mvc.perform(post("/registration")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(TestUtils.MYUSER0_DTO.getId()))
                .andExpect(jsonPath("$.username", is(TestUtils.MYUSER0_DTO.getUsername())))
                .andExpect(jsonPath("$.password", is(TestUtils.MYUSER0_DTO.getPassword())))
                .andExpect(jsonPath("$.email", is(TestUtils.MYUSER0_DTO.getEmail())))
                .andExpect(jsonPath("$.role", is(TestUtils.MYUSER0_DTO.getRole().toString())));
    }
}