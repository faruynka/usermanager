package com.user_manager.controller;

import com.user_manager.domain.User;
import com.user_manager.dto.RequestUserDto;
import com.user_manager.service.UserService;
import com.user_manager.transformer.Transformer;
import com.user_manager.utils.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AdminController.class)
class AdminControllerTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private AdminController controller;

    @MockBean
    private UserService userService;
    @MockBean
    private Transformer<User, RequestUserDto> userTransformer;

    @Test
    void getUsersTest() throws Exception {
        List<User> user_list = new ArrayList<User>(Arrays.asList(TestUtils.MYUSER0));
        List<RequestUserDto> request_user_dto_list = new ArrayList<RequestUserDto>(Arrays.asList(
                TestUtils.REQUEST_MYUSER0_DTO));
        given(userService.findActive(TestUtils.MYUSER0.getId()))
                .willReturn(user_list);
        given(userTransformer.parseToDto(user_list))
                .willReturn(request_user_dto_list);

        mvc.perform(get("/admin")
                .header(HttpHeaders.AUTHORIZATION, TestUtils.MYUSER0.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(TestUtils.REQUEST_MYUSER0_DTO.getId()))
                .andExpect(jsonPath("$[0].username", is(TestUtils.REQUEST_MYUSER0_DTO.getUsername())))
                .andExpect(jsonPath("$[0].password", is(TestUtils.REQUEST_MYUSER0_DTO.getPassword())))
                .andExpect(jsonPath("$[0].repeatedPassword", is(TestUtils.REQUEST_MYUSER0_DTO.getRepeatedPassword())))
                .andExpect(jsonPath("$[0].email", is(TestUtils.REQUEST_MYUSER0_DTO.getEmail())))
                .andExpect(jsonPath("$[0].role", is(TestUtils.REQUEST_MYUSER0_DTO.getRole().toString())));
    }
}