package com.user_manager.controller;

import com.user_manager.domain.User;
import com.user_manager.dto.AuthenticateUserRequestDto;
import com.user_manager.dto.UserDto;
import com.user_manager.service.AuthenticationService;
import com.user_manager.transformer.Transformer;
import com.user_manager.utils.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(AuthenticationController.class)
class AuthenticationControllerTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private AuthenticationController controller;

    @MockBean
    private AuthenticationService authenticationService;

    @MockBean
    private Transformer<User, AuthenticateUserRequestDto> authenticateUserTransformer;

    @MockBean
    private Transformer<User, UserDto> userTransformer;

    @Test
    void loginTest() throws Exception {
        mvc.perform(get("/authentication"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello user. Log in, please.")));
    }

    @Test
    void loginDtoTest() throws Exception {
        String userRequest = "{\"username\": \"" + TestUtils.AUTHENTICATE_MYUSER0_REQUEST_DTO.getUsername() + "\"," +
                " \"password\" : \"" + TestUtils.AUTHENTICATE_MYUSER0_REQUEST_DTO.getPassword() + "\"}";

        given(authenticateUserTransformer.parseToEntity(TestUtils.AUTHENTICATE_MYUSER0_REQUEST_DTO))
                .willReturn(TestUtils.MYUSER0);
        given(authenticationService.login(TestUtils.MYUSER0)).willReturn(TestUtils.MYUSER0);
        given(userTransformer.parseToDto(TestUtils.MYUSER0)).willReturn(TestUtils.MYUSER0_DTO);

        mvc.perform(post("/authentication/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userRequest))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(TestUtils.MYUSER0_DTO.getId()))
                .andExpect(jsonPath("$.username", is(TestUtils.MYUSER0_DTO.getUsername())))
                .andExpect(jsonPath("$.password", is(TestUtils.MYUSER0_DTO.getPassword())))
                .andExpect(jsonPath("$.email", is(TestUtils.MYUSER0_DTO.getEmail())))
                .andExpect(jsonPath("$.role", is(TestUtils.MYUSER0_DTO.getRole().toString())));
    }
}