package com.user_manager.controller;

import com.user_manager.domain.User;
import com.user_manager.dto.PasswordUserValidationDto;
import com.user_manager.dto.UserDto;
import com.user_manager.service.UserService;
import com.user_manager.transformer.Transformer;
import com.user_manager.utils.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserSettingsController.class)
class UserSettingsControllerTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private UserSettingsController controller;

    @MockBean
    private UserService userService;
    @MockBean
    private Transformer<User, UserDto> userTransformer;
    @MockBean
    private Transformer<User, PasswordUserValidationDto> passwordValidateTransformer;

    @Test
    void getTest() throws Exception {
        String userDto = "{\"id\": \"" + TestUtils.MYUSER0_DTO.getId() + "\"," +
                "\"username\": \"" + TestUtils.MYUSER0_DTO.getUsername() + "\"," +
                " \"password\" : \"" + TestUtils.MYUSER0_DTO.getPassword() + "\"," +
                " \"email\" : \"" + TestUtils.MYUSER0_DTO.getEmail() + "\"," +
                " \"role\" : \"" + TestUtils.MYUSER0_DTO.getRole() + "\"}";

        given(userService.find(TestUtils.MYUSER1.getId(), TestUtils.MYUSER0_DTO.getId()))
                .willReturn(TestUtils.MYUSER1);
        given(userTransformer.parseToDto(TestUtils.MYUSER1))
                .willReturn(TestUtils.MYUSER1_DTO);

        mvc.perform(get("/settings/user")
                .header(HttpHeaders.AUTHORIZATION, TestUtils.MYUSER1.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(userDto))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(TestUtils.REQUEST_MYUSER1_DTO.getId()))
                .andExpect(jsonPath("$.username", is(TestUtils.REQUEST_MYUSER1_DTO.getUsername())))
                .andExpect(jsonPath("$.password", is(TestUtils.REQUEST_MYUSER1_DTO.getPassword())))
                .andExpect(jsonPath("$.email", is(TestUtils.REQUEST_MYUSER1_DTO.getEmail())))
                .andExpect(jsonPath("$.role", is(TestUtils.REQUEST_MYUSER1_DTO.getRole().toString())));
    }

    @Test
    void editUserTest() throws Exception {
        String userDto = "{\"id\": \"" + TestUtils.MYUSER0_DTO.getId() + "\"," +
                "\"username\": \"" + TestUtils.MYUSER0_DTO.getUsername() + "\"," +
                " \"password\" : \"" + TestUtils.MYUSER0_DTO.getPassword() + "\"," +
                " \"email\" : \"" + TestUtils.MYUSER0_DTO.getEmail() + "\"," +
                " \"role\" : \"" + TestUtils.MYUSER0_DTO.getRole() + "\"}";

        given(userTransformer.parseToEntity(TestUtils.MYUSER0_DTO))
                .willReturn(TestUtils.MYUSER0);
        given(userService.edit(TestUtils.MYUSER1.getId(), TestUtils.MYUSER0))
                .willReturn(TestUtils.MYUSER0);
        given(userTransformer.parseToDto(TestUtils.MYUSER0))
                .willReturn(TestUtils.MYUSER0_DTO);

        mvc.perform(post("/settings/user/edit")
                .header(HttpHeaders.AUTHORIZATION, TestUtils.MYUSER1.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(userDto))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(TestUtils.REQUEST_MYUSER0_DTO.getId()))
                .andExpect(jsonPath("$.username", is(TestUtils.REQUEST_MYUSER0_DTO.getUsername())))
                .andExpect(jsonPath("$.password", is(TestUtils.REQUEST_MYUSER0_DTO.getPassword())))
                .andExpect(jsonPath("$.email", is(TestUtils.REQUEST_MYUSER0_DTO.getEmail())))
                .andExpect(jsonPath("$.role", is(TestUtils.REQUEST_MYUSER0_DTO.getRole().toString())));
    }

    @Test
    void deleteUserTest() throws Exception {
        String userDto = "{\"id\": \"" + TestUtils.MYUSER0_DTO.getId() + "\"," +
                "\"username\": \"" + TestUtils.MYUSER0_DTO.getUsername() + "\"," +
                " \"password\" : \"" + TestUtils.MYUSER0_DTO.getPassword() + "\"," +
                " \"email\" : \"" + TestUtils.MYUSER0_DTO.getEmail() + "\"," +
                " \"role\" : \"" + TestUtils.MYUSER0_DTO.getRole() + "\"}";

        given(userTransformer.parseToEntity(TestUtils.MYUSER0_DTO))
                .willReturn(TestUtils.MYUSER0);
        given(userService.delete(TestUtils.MYUSER0.getId(), TestUtils.MYUSER0.getId(), TestUtils.MYUSER0.getPassword()))
                .willReturn(TestUtils.MYUSER0);
        given(userTransformer.parseToDto(TestUtils.MYUSER0))
                .willReturn(TestUtils.MYUSER0_DTO);

        mvc.perform(delete("/settings/user/delete")
                .header(HttpHeaders.AUTHORIZATION, TestUtils.MYUSER0.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(userDto))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(TestUtils.REQUEST_MYUSER0_DTO.getId()))
                .andExpect(jsonPath("$.username", is(TestUtils.REQUEST_MYUSER0_DTO.getUsername())))
                .andExpect(jsonPath("$.password", is(TestUtils.REQUEST_MYUSER0_DTO.getPassword())))
                .andExpect(jsonPath("$.email", is(TestUtils.REQUEST_MYUSER0_DTO.getEmail())))
                .andExpect(jsonPath("$.role", is(TestUtils.REQUEST_MYUSER0_DTO.getRole().toString())));
    }

    @Test
    void recover() throws Exception {
        String passwordValidationUserDto
                = "{\"username\": \"" + TestUtils.PASSWORD_VALIDATION_MYUSER0_DTO.getUsername() + "\"," +
                " \"password\" : \"" + TestUtils.PASSWORD_VALIDATION_MYUSER0_DTO.getPassword() + "\"," +
                " \"repeatedPassword\" : \"" + TestUtils.PASSWORD_VALIDATION_MYUSER0_DTO.getRepeatedPassword() + "\"}";

        given(passwordValidateTransformer.parseToEntity(TestUtils.PASSWORD_VALIDATION_MYUSER0_DTO))
                .willReturn(TestUtils.MYUSER0);
        given(userService.recover(TestUtils.MYUSER0.getId(), TestUtils.MYUSER0,
                TestUtils.PASSWORD_VALIDATION_MYUSER0_DTO.getRepeatedPassword())).willReturn(TestUtils.MYUSER0);
        given(userTransformer.parseToDto(TestUtils.MYUSER0))
                .willReturn(TestUtils.MYUSER0_DTO);

        mvc.perform(put("/settings/user/recover")
                .header(HttpHeaders.AUTHORIZATION, TestUtils.MYUSER0.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(passwordValidationUserDto))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(TestUtils.REQUEST_MYUSER0_DTO.getId()))
                .andExpect(jsonPath("$.username", is(TestUtils.REQUEST_MYUSER0_DTO.getUsername())))
                .andExpect(jsonPath("$.password", is(TestUtils.REQUEST_MYUSER0_DTO.getPassword())))
                .andExpect(jsonPath("$.email", is(TestUtils.REQUEST_MYUSER0_DTO.getEmail())))
                .andExpect(jsonPath("$.role", is(TestUtils.REQUEST_MYUSER0_DTO.getRole().toString())));
    }
}