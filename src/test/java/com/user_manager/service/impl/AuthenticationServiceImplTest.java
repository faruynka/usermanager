package com.user_manager.service.impl;

import com.user_manager.domain.User;
import com.user_manager.exception.PasswordException;
import com.user_manager.repo.UserRepo;
import com.user_manager.service.AuthenticationService;
import com.user_manager.utils.TestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AuthenticationServiceImpl.class})
class AuthenticationServiceImplTest {
    @Autowired
    private AuthenticationService authenticationService;

    @MockBean
    private UserRepo userRepo;

    @Test
    void loginTest() {
        Mockito.when(userRepo.findByUsernameAndIsDeletedFalse(TestUtils.MYUSER0.getUsername()))
                .thenReturn(Optional.of(TestUtils.MYUSER0));
        assertEquals(TestUtils.MYUSER0, authenticationService.login(TestUtils.MYUSER0));
    }

    @Test
    void loginTestEntityNotFound() {
        Mockito.when(userRepo.findByUsernameAndIsDeletedFalse(TestUtils.MYUSER0.getUsername()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class,
                ()->authenticationService.login(TestUtils.MYUSER0));
    }

    @Test
    void loginTestEntityNotFoundDeleted() {
        User deletedUser = new User(TestUtils.MYUSER0.getId(), "DELETED",
                TestUtils.MYUSER0.getRealUsername(), TestUtils.MYUSER0.getPassword(), TestUtils.MYUSER0.getEmail(),
                true, TestUtils.MYUSER0.getRole());
        Mockito.when(userRepo.findByUsernameAndIsDeletedFalse(TestUtils.MYUSER0.getUsername()))
                .thenReturn(Optional.of(deletedUser));
        Assertions.assertThrows(EntityNotFoundException.class,
                ()->authenticationService.login(deletedUser));
    }

    @Test
    void loginTestPasswordException() {
        User wrongPasswordUser = new User(TestUtils.MYUSER0.getId(), TestUtils.MYUSER0.getUsername(),
                TestUtils.MYUSER0.getRealUsername(), TestUtils.MYUSER0.getPassword() + "abc",
                TestUtils.MYUSER0.getEmail(), TestUtils.MYUSER0.getDeleted(), TestUtils.MYUSER0.getRole());
        Mockito.when(userRepo.findByUsernameAndIsDeletedFalse(TestUtils.MYUSER0.getUsername()))
                .thenReturn(Optional.of(wrongPasswordUser));
        Assertions.assertThrows(PasswordException.class,
                ()->authenticationService.login(TestUtils.MYUSER0));
    }
}