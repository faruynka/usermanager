package com.user_manager.service.impl;

import com.user_manager.domain.User;
import com.user_manager.exception.BadRequestException;
import com.user_manager.exception.PasswordException;
import com.user_manager.repo.UserRepo;
import com.user_manager.service.UserService;
import com.user_manager.utils.TestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityNotFoundException;
import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {UserServiceImpl.class})
class UserServiceImplTest {
    @Autowired
    private UserService userService;

    @MockBean
    private UserRepo userRepo;

    @Test
    void findTestSelf() {
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER0.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER0));
        assertEquals(TestUtils.MYUSER0, userService.find(TestUtils.MYUSER0.getId()));
    }

    @Test
    void findTestSelfEntityNotFound() {
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER0.getId()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class,
                ()->userService.find(TestUtils.MYUSER0.getId()));
    }

    @Test
    void findTestAdmin() throws AccessDeniedException {
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER2.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER2));
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER0.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER0));
        assertEquals(TestUtils.MYUSER0, userService.find(TestUtils.MYUSER2.getId(), TestUtils.MYUSER0.getId()));
    }

    @Test
    void findTestAccessDenied() {
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER1.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER1));
        Assertions.assertThrows(AccessDeniedException.class,
                ()->userService.find(TestUtils.MYUSER1.getId(), TestUtils.MYUSER0.getId()));
    }

    @Test
    void findTestEntityNotFound1() {
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER2.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER2));
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER0.getId()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class,
                ()->userService.find(TestUtils.MYUSER1.getId(), TestUtils.MYUSER0.getId()));
    }

    @Test
    void findTestEntityNotFound2() {
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER2.getId()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class,
                ()->userService.find(TestUtils.MYUSER1.getId(), TestUtils.MYUSER0.getId()));
    }

    @Test
    void findActiveTestAdmin() throws AccessDeniedException {
        List<User> userList = new ArrayList<User>(Arrays.asList(TestUtils.MYUSER0,
                TestUtils.MYUSER1, TestUtils.MYUSER2));
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER2.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER2));
        Mockito.when(userRepo.findAllByIsDeletedIsFalse()).thenReturn(userList);
        assertEquals(userList, userService.findActive(TestUtils.MYUSER2.getId()));
    }

    @Test
    void findActiveTestUser() {
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER0.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER0));
        Assertions.assertThrows(AccessDeniedException.class, ()->userService.findActive(TestUtils.MYUSER0.getId()));
    }
    
    @Test
    void addTest() {
        Mockito.when(userRepo.findByUsernameAndIsDeletedFalse(TestUtils.MYUSER0.getUsername())).
                thenReturn(Optional.empty());
        Mockito.when(userRepo.save(TestUtils.MYUSER0)).
                thenReturn(TestUtils.MYUSER0);
        assertEquals(TestUtils.MYUSER0, userService.add(TestUtils.MYUSER0, TestUtils.MYUSER0.getPassword()));
    }

    @Test
    void addTestAlreadyExists() {
        Mockito.when(userRepo.findByUsernameAndIsDeletedFalse(TestUtils.MYUSER0.getUsername())).
                thenReturn(Optional.of(TestUtils.MYUSER0));
        Assertions.assertThrows(EntityNotFoundException.class,
                ()->userService.add(TestUtils.MYUSER0, TestUtils.MYUSER0.getPassword()));
    }

    @Test
    void addTestPasswordException() {
        Assertions.assertThrows(PasswordException.class,
                ()->userService.add(TestUtils.MYUSER0, TestUtils.MYUSER1.getPassword()));
    }

    @Test
    void editTestAdmin() throws AccessDeniedException {
        User user = new User(TestUtils.MYUSER0.getId(), TestUtils.MYUSER0.getUsername(),
                TestUtils.MYUSER0.getRealUsername(), TestUtils.MYUSER0.getPassword(), TestUtils.MYUSER0.getEmail(),
                TestUtils.MYUSER0.getDeleted(), TestUtils.MYUSER0.getRole());
        User userChanged = new User(TestUtils.MYUSER0.getId(), TestUtils.MYUSER0.getUsername(),
                TestUtils.MYUSER0.getRealUsername(), TestUtils.MYUSER0.getPassword(), "myuser0NewMail@gmail.com",
                TestUtils.MYUSER0.getDeleted(), TestUtils.MYUSER0.getRole());
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER2.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER2));
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(userChanged.getId()))
                .thenReturn(Optional.of(user));
        Mockito.when(userRepo.save(user)).
                thenReturn(user);
        assertEquals(userChanged, userService.edit(TestUtils.MYUSER2.getId(), userChanged));
    }

    @Test
    void editTestSelf() throws AccessDeniedException {
        User user = new User(TestUtils.MYUSER0.getId(), TestUtils.MYUSER0.getUsername(),
                TestUtils.MYUSER0.getRealUsername(), TestUtils.MYUSER0.getPassword(), TestUtils.MYUSER0.getEmail(),
                TestUtils.MYUSER0.getDeleted(), TestUtils.MYUSER0.getRole());
        User userChanged = new User(TestUtils.MYUSER0.getId(), TestUtils.MYUSER0.getUsername(),
                TestUtils.MYUSER0.getRealUsername(), TestUtils.MYUSER0.getPassword(), "myuser0NewMail@gmail.com",
                TestUtils.MYUSER0.getDeleted(), TestUtils.MYUSER0.getRole());
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(user.getId()))
                .thenReturn(Optional.of(user));
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(userChanged.getId()))
                .thenReturn(Optional.of(user));
        Mockito.when(userRepo.save(user)).
                thenReturn(user);
        assertEquals(userChanged, userService.edit(user.getId(), userChanged));
    }

    @Test
    void editTestAccessDenied() {
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER0.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER0));
        Assertions.assertThrows(AccessDeniedException.class,
                ()->userService.edit(TestUtils.MYUSER0.getId(), TestUtils.MYUSER1));
    }

    @Test
    void editTestEntityNotFound1() {
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER2.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER2));
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER0.getId()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class,
                ()->userService.edit(TestUtils.MYUSER2.getId(), TestUtils.MYUSER0));
    }

    @Test
    void editTestEntityNotFound2() {
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER2.getId()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class,
                ()->userService.edit(TestUtils.MYUSER2.getId(), TestUtils.MYUSER0));
    }

    @Test
    void editTestBadRequest() {
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER2.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER2));
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER0.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER0));
        Mockito.when(userRepo.save(TestUtils.MYUSER0)).
                thenReturn(TestUtils.MYUSER0);
        Assertions.assertThrows(BadRequestException.class,
                ()->userService.edit(TestUtils.MYUSER2.getId(), TestUtils.MYUSER0));
    }

    @Test
    void deleteTestAdmin() throws AccessDeniedException {
        User userDeleted = new User(TestUtils.MYUSER0.getId(), "DELETED",
                TestUtils.MYUSER0.getRealUsername(), TestUtils.MYUSER0.getPassword(), TestUtils.MYUSER0.getEmail(),
                true, TestUtils.MYUSER0.getRole());
        User user = new User(TestUtils.MYUSER0.getId(), TestUtils.MYUSER0.getUsername(),
                TestUtils.MYUSER0.getRealUsername(), TestUtils.MYUSER0.getPassword(), TestUtils.MYUSER0.getEmail(),
                TestUtils.MYUSER0.getDeleted(), TestUtils.MYUSER0.getRole());
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER2.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER2));
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(user.getId()))
                .thenReturn(Optional.of(user));
        Mockito.when(userRepo.save(user)).
                thenReturn(user);
        assertEquals(userDeleted, userService.delete(TestUtils.MYUSER2.getId(), user.getId(), user.getPassword()));
    }

    @Test
    void deleteTestSelf() throws AccessDeniedException {
        User userDeleted = new User(TestUtils.MYUSER0.getId(), "DELETED",
                TestUtils.MYUSER0.getRealUsername(), TestUtils.MYUSER0.getPassword(), TestUtils.MYUSER0.getEmail(),
                true, TestUtils.MYUSER0.getRole());
        User user = new User(TestUtils.MYUSER0.getId(), TestUtils.MYUSER0.getUsername(),
                TestUtils.MYUSER0.getRealUsername(), TestUtils.MYUSER0.getPassword(), TestUtils.MYUSER0.getEmail(),
                TestUtils.MYUSER0.getDeleted(), TestUtils.MYUSER0.getRole());
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(user.getId()))
                .thenReturn(Optional.of(user));
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(user.getId()))
                .thenReturn(Optional.of(user));
        Mockito.when(userRepo.save(user)).
                thenReturn(user);
        assertEquals(userDeleted, userService.delete(user.getId(), user.getId(), user.getPassword()));
    }

    @Test
    void deleteTestAccessDenied() {
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER0.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER0));
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER1.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER1));
        Assertions.assertThrows(AccessDeniedException.class,
                ()->userService.delete(TestUtils.MYUSER0.getId(), TestUtils.MYUSER1.getId(),
                        TestUtils.MYUSER1.getPassword()));
    }

    @Test
    void deleteTestEntityNotFound1() {
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER2.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER2));
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER1.getId()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class,
                ()->userService.delete(TestUtils.MYUSER2.getId(), TestUtils.MYUSER1.getId(),
                        TestUtils.MYUSER1.getPassword()));
    }

    @Test
    void deleteTestEntityNotFound2() {
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER2.getId()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class,
                ()->userService.delete(TestUtils.MYUSER2.getId(), TestUtils.MYUSER1.getId(),
                        TestUtils.MYUSER1.getPassword()));
    }

    @Test
    void deleteTestPasswordException() {
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER2.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER2));
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER1.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER1));
        Assertions.assertThrows(PasswordException.class,
                ()->userService.delete(TestUtils.MYUSER2.getId(), TestUtils.MYUSER1.getId(),
                        TestUtils.MYUSER0.getPassword()));
    }

    @Test
    void deleteAllByStatus() {
        Assertions.assertDoesNotThrow(()->userService.deleteAllByStatus());
    }

    @Test
    void recoverTestAdmin() throws AccessDeniedException {
        User userDeleted = new User(TestUtils.MYUSER0.getId(), "DELETED",
                TestUtils.MYUSER0.getRealUsername(), TestUtils.MYUSER0.getPassword(), TestUtils.MYUSER0.getEmail(),
                true, TestUtils.MYUSER0.getRole());
        User userRecovered = new User(TestUtils.MYUSER0.getId(), TestUtils.MYUSER0.getUsername(),
                TestUtils.MYUSER0.getRealUsername(), TestUtils.MYUSER0.getPassword(), TestUtils.MYUSER0.getEmail(),
                TestUtils.MYUSER0.getDeleted(), TestUtils.MYUSER0.getRole());
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER2.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER2));
        Mockito.when(userRepo.findByRealUsername(userDeleted.getUsername()))
                .thenReturn(Optional.of(userDeleted));
        Mockito.when(userRepo.save(userDeleted)).
                thenReturn(userDeleted);
        assertEquals(userRecovered, userService.recover(TestUtils.MYUSER2.getId(), userDeleted,
                userDeleted.getPassword()));
    }

    @Test
    void recoverTestSelf() throws AccessDeniedException {
        User userDeleted = new User(TestUtils.MYUSER0.getId(), "DELETED",
                TestUtils.MYUSER0.getRealUsername(), TestUtils.MYUSER0.getPassword(), TestUtils.MYUSER0.getEmail(),
                true, TestUtils.MYUSER0.getRole());
        User userRecovered = new User(TestUtils.MYUSER0.getId(), TestUtils.MYUSER0.getUsername(),
                TestUtils.MYUSER0.getRealUsername(), TestUtils.MYUSER0.getPassword(), TestUtils.MYUSER0.getEmail(),
                TestUtils.MYUSER0.getDeleted(), TestUtils.MYUSER0.getRole());
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(userDeleted.getId()))
                .thenReturn(Optional.of(userDeleted));
        Mockito.when(userRepo.findByRealUsername(userDeleted.getUsername()))
                .thenReturn(Optional.of(userDeleted));
        Mockito.when(userRepo.save(userDeleted)).
                thenReturn(userDeleted);
        assertEquals(userRecovered, userService.recover(userDeleted.getId(), userDeleted,
                userDeleted.getPassword()));
    }

    @Test
    void recoverTestAccessDenied() {
        User userDeleted = new User(TestUtils.MYUSER0.getId(), "DELETED",
                TestUtils.MYUSER0.getRealUsername(), TestUtils.MYUSER0.getPassword(), TestUtils.MYUSER0.getEmail(),
                true, TestUtils.MYUSER0.getRole());
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER1.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER1));
        Assertions.assertThrows(AccessDeniedException.class,
                ()->userService.recover(TestUtils.MYUSER1.getId(), userDeleted,
                        userDeleted.getPassword()));
    }

    @Test
    void recoverTestEntityNotFound1() {
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER2.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER2));
        Mockito.when(userRepo.findByRealUsername(TestUtils.MYUSER0.getUsername()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class,
                ()->userService.recover(TestUtils.MYUSER2.getId(), TestUtils.MYUSER0,
                        TestUtils.MYUSER0.getPassword()));
    }

    @Test
    void recoverTestEntityNotFound2() {
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER2.getId()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class,
                ()->userService.recover(TestUtils.MYUSER2.getId(), TestUtils.MYUSER0,
                        TestUtils.MYUSER0.getPassword()));
    }

    @Test
    void recoverTestPasswordException() {
        User userDeleted = new User(TestUtils.MYUSER0.getId(), "DELETED",
                TestUtils.MYUSER0.getRealUsername(), TestUtils.MYUSER0.getPassword(), TestUtils.MYUSER0.getEmail(),
                true, TestUtils.MYUSER0.getRole());
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER2.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER2));
        Assertions.assertThrows(PasswordException.class,
                ()->userService.recover(TestUtils.MYUSER2.getId(), userDeleted,
                        TestUtils.MYUSER1.getPassword()));
    }

    @Test
    void recoverTestBadRequest() {
        User user = new User(TestUtils.MYUSER0.getId(), TestUtils.MYUSER0.getUsername(),
                TestUtils.MYUSER0.getRealUsername(), TestUtils.MYUSER0.getPassword(), TestUtils.MYUSER0.getEmail(),
                TestUtils.MYUSER0.getDeleted(), TestUtils.MYUSER0.getRole());
        Mockito.when(userRepo.findByIdAndIsDeletedFalse(TestUtils.MYUSER2.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER2));
        Mockito.when(userRepo.findByRealUsername(user.getUsername()))
                .thenReturn(Optional.of(user));
        Assertions.assertThrows(BadRequestException.class,
                ()->userService.recover(TestUtils.MYUSER2.getId(), user,
                        user.getPassword()));
    }
}